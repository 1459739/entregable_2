/* Procedimientos */

/* Procedimiento para insertar */
CREATE PROCEDURE InsertarArticulo
    @idcategoria INT,
    @codigo VARCHAR(50),
    @nombre VARCHAR(100),
    @precio_venta DECIMAL(11, 2),
    @stock INT,
    @descripcion VARCHAR(255),
    @imagen VARCHAR(100)
AS
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen, estado)
    VALUES (@idcategoria, @codigo, @nombre, @precio_venta, @stock, @descripcion, @imagen, 1);
END;
GO

/* Procedimiento para actualizar */
CREATE PROCEDURE ActualizarArticulo
    @idarticulo INT,
    @nombre VARCHAR(100),
    @precio_venta DECIMAL(11, 2),
    @stock INT
AS
BEGIN
    UPDATE articulo
    SET nombre = @nombre, precio_venta = @precio_venta, stock = @stock
    WHERE idarticulo = @idarticulo;
END;
GO

/* Procedimiento para eliminar */
CREATE PROCEDURE EliminarArticulo
    @idarticulo INT
AS
BEGIN
    DELETE FROM articulo WHERE idarticulo = @idarticulo;
END;
GO


/* Manipulaciones */

/* Combinacion 1 */
SELECT nombre FROM articulo
UNION
SELECT nombre FROM categoria;

/* Combinacion 2 */
SELECT nombre FROM articulo
UNION ALL
SELECT nombre FROM categoria;

/* Interserccion */
SELECT nombre FROM articulo
INTERSECT
SELECT nombre FROM categoria;

/* Exepcion */
SELECT nombre FROM articulo
EXCEPT
SELECT nombre FROM categoria;


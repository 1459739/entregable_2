/* GROUP BY y HAVING */

/* Ejemplo 1 */
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;

/* Ejemplo 2*/
SELECT idusuario, COUNT(*) AS ventas_completadas
FROM venta
WHERE estado = 'Pendiente'
GROUP BY idusuario
HAVING COUNT(*) >= 1;
